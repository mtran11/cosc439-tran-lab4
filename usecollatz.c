#include <stdio.h> 
#include <sys/types.h> 
#include <unistd.h>  
#include <stdlib.h> 
#include <errno.h>   
#include <sys/wait.h>

int ChildProcess(); /* child process prototype */ 
int ParentProcess(); /* parent process prototype */ 

int main(){ 	
	pid_t  pid; 
	pid = fork(); 
  
	if (pid == 0){ 
		ChildProcess(); 
	}else { 
		ParentProcess();
	}
	return 0; 
}
int ChildProcess() {
	char number[100];
	scanf("%s", number);
	while (atoi(number) > 1 || atoi(number) < 100) {
		char *args[] = {"./collatz", number, NULL}; 
		execv(args[0], args);
	}
} 
int ParentProcess() {
	printf("Please enter number between [1-100]:\n");
	wait(NULL);		
}
