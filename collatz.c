#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include<sys/wait.h> 
#include<unistd.h>  

int ChildProcess(); /* child process prototype */ 
int ParentProcess(); /* parent process prototype */ 

int main(int argc, char *argv[]){
	unsigned int number;
	number = atoi(argv[1]); //convert string to int
	
	pid_t pid; 
	pid = fork(); 

	if (pid == 0){ 
		ChildProcess(number); 
	}else { 
		ParentProcess(number);
	}
	return 0; 
} 

int ChildProcess(unsigned int n) {
	printf("%d\n",n); 
	while (n != 1) {
		if (n % 2 == 0){
			n = n/2;
		}else {
			n = 3*n+1;
		}
		printf("%d\n",n);
	}
} 
int ParentProcess(int n) {
	printf("START\n");
	if (n <= 0){
		printf("Please input the positive number!\n");
		kill(0,SIGKILL); //stop program if user input negative number
	} else {
		wait(NULL);
	}
	printf("END\n");
}
